name := """play-offers"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala).
				settings(
					slick <<= slickCodeGenTask
				).dependsOn(codegen)
				
lazy val codegen = project.settings(
	scalaVersion := "2.11.2",
	libraryDependencies ++= List(
        "com.typesafe.slick" %% "slick-codegen" % "2.1.0",
        "org.scala-lang" % "scala-reflect" % "2.11.2"
      ))

scalaVersion := "2.11.2"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.typesafe.play" %% "play-slick" % "0.8.0",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "com.typesafe.slick" %% "slick-codegen" % "2.1.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "mysql" % "mysql-connector-java" % "5.1.31",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "bootstrap" % "3.2.0",
  "org.webjars" % "angularjs" % "1.2.21",
  "org.webjars" % "jquery" % "2.1.1",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.webjars" % "notifyjs" % "0.3.2",
  "org.seleniumhq.selenium" % "selenium-java" % "2.43.0"
)
