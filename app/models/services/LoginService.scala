package models.services

import play.api.db._
import models.Tables._
import scala.slick.jdbc.JdbcBackend.Database
import scala.slick.driver.MySQLDriver.simple._
import javax.sql.DataSource
import org.mindrot.jbcrypt.BCrypt

import models.gen.repositories.LoginRepo

trait LoginService {
  
  def checkCredentials(username: String, password: String): Boolean

  def saveUser(user: UsersRow): Either[String,UsersRow]

  def userExists(username: String): Boolean

  def getUser(username: String): Option[UsersRow]
  
  def getUser(id: Int): Option[UsersRow]
  
}

class LoginServiceImp(datasource: DataSource, val loginRepo: LoginRepo) extends LoginService{
  
 private val slickDb = Database.forDataSource(datasource)

  def checkCredentials(username: String, password: String): Boolean = {
    
    slickDb.withSession{
      implicit session =>
        loginRepo.checkCredentials(username, password).apply(session)
    }
  }

  def saveUser(user: UsersRow) = {
    slickDb.withTransaction {
      implicit session =>
        val userEncripted = user.copy(password = createPasswd(user.password))
        loginRepo.add(userEncripted).apply(session)
    }
  }

  def createPasswd(clearString: String) = {
    BCrypt.hashpw(clearString, BCrypt.gensalt())
  }

  def userExists(username: String) = {
    slickDb.withSession{
      implicit session =>
        loginRepo.userExists(username).apply(session)
    }
  }

  def getUser(username: String): Option[UsersRow] = {
    slickDb.withSession { implicit session =>
      loginRepo.getUser(username).apply(session)
    }
  }
  
  def getUser(id: Int): Option[UsersRow] = slickDb.withSession { implicit session =>
      loginRepo.findById(id).apply(session) 
    }
}