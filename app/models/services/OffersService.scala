package models.services

import scala.slick.driver.MySQLDriver.simple._
import models.Tables._

import models.gen.repositories.OffersRepo

import javax.sql.DataSource

trait OffersService {
  def save(offer: OffersRow): Either[String, OffersRow]
  
  def update(offer: OffersRow): Either[String, OffersRow]
  
  def list: List[(OffersRow, UsersRow)]

  def byId(id: Int): Option[OffersRow]
  
  def offers(userid: Int): List[(OffersRow, String)]
  
  def delete(offer: OffersRow): Either[String, Unit]
}

class OffersServiceImp(ds: DataSource, val offersRepo: OffersRepo) extends OffersService {
  private val slickDb = Database.forDataSource(ds)
  
  def save(offer: OffersRow) =  slickDb.withSession { implicit session =>
    offersRepo.add(offer).apply(session)
  }
  
  def update(offer: OffersRow) = slickDb.withSession { implicit session =>
    offersRepo.update(offer).apply(session)
  }
  
  def list = slickDb.withSession { implicit session => 
    offersRepo.validWithEmail.apply(session)
  }

  def byId(id: Int) =  slickDb.withSession { implicit session => 
    offersRepo.findById(id).apply(session)
  }
  
  def offers(userid: Int) = slickDb.withSession { implicit session => 
    offersRepo.offersByUser(userid).apply(session)
  }  
  
  def delete(offer: OffersRow) = slickDb.withSession { implicit session =>
     offersRepo.deleteById(offer.id.get).apply(session)
  }
}