package models.gen.repositories

import scala.slick.driver.JdbcDriver.simple._

import models.Tables._
import scala.language.reflectiveCalls

import models.DBReader

//Base repository for standard generator (without Option for id nor bases traits)
abstract class SimpleRepo[I:BaseColumnType, T <: AnyRef { val id: I }] {
    val profile: scala.slick.driver.MySQLDriver
    import profile.simple._ 

  val query: TableQuery[_ <: Table[T] { val id: Column[I] }]
  
  def withId(model: T, id: I): T

  def add(model: T): DBReader[Either[String, T]] = DBReader { implicit s =>
    try {
      val id = query.returning(query.map(_.id)).insert(model)
      Right(withId(model, id))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def update(model: T): DBReader[Either[String, T]] = DBReader { implicit s =>
    try {
      val res = filterById(model.id).update(model)
      if (res > 0)
        Right(model)
      else
        Left("Nothing saved")
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def list: DBReader[List[T]] = DBReader { implicit s => query.list }

  def pagedList(pageIndex: Int, limit: Int): DBReader[List[T]] = DBReader { implicit s =>
    query.drop(pageIndex).take(limit).run.toList
  }

  def filterById(id: I) = query.filter(_.id === id)

  def deleteById(id: I): DBReader[Either[String, Unit]] = DBReader { implicit s =>
    try {
      val res = filterById(id).delete
      Right(())
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def findById(id: I): DBReader[Option[T]] = DBReader { implicit s =>
    filterById(id).firstOption
  }
}

/* Example of usage
 * */
//trait MessagesRepo extends SimpleRepo[Int, MessagesRow] {
//  val profile = scala.slick.driver.MySQLDriver
//  
//  val query  = Messages
//  
//  def withId(model: MessagesRow, id: Int) = model.copy(id = id)
//
//  def byEmail(email: String): DB[List[MessagesRow]] = DB { implicit s =>
//    val res = for {
//      m <- query if (m.email === email)
//    } yield (m)
//    res.list
//  }
//
//  def byEmail2(email: String) =
//    for {
//      m <- query if (m.email === email)
//    } yield (m)
//
//  def byEmailAndSomething(email: String, som: String) = {
//    val q = byEmail2(email)
//    for {
//      m <- q if (m.subject === som)
//      y <- m.usersFk
//    } yield (m, y)
//  }
//}
