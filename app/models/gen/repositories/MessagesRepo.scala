package models.gen.repositories

import models.Tables._
import models.domain.PostgresProfile
import models.DBReader

import models.domain.TableWithId

trait MessagesRepo extends BaseRepo[Int, MessagesRow] with PostgresProfile{
  import profile.simple._
  val query: TableQuery[_ <: Table[MessagesRow] with TableWithId[Int]] = Messages
  
  def withId(model: MessagesRow, id: Int) = model.copy(id = Some(id))
  
  def byUser(user : UsersRow): DBReader[List[MessagesRow]] = DBReader{ implicit s =>
    user.id.map(byUserId(_).apply(s)).getOrElse(List())
  }
  
  def byUserId(userid : Int): DBReader[List[MessagesRow]] = DBReader{ implicit s =>
    Messages.filter(_.userid === userid).list 
  }
  
  def listWithUsers(userid : Int): DBReader[List[(MessagesRow, UsersRow)]] = DBReader { implicit s =>
    (for{
      msg <- Messages if( msg.userid === userid)
      user <- msg.usersFk2
    }yield(msg, user)).list
  }
}