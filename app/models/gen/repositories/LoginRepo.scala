package models.gen.repositories

import models.Tables._
import models.domain.PostgresProfile

import models.DBReader
import org.mindrot.jbcrypt.BCrypt

trait LoginRepo extends BaseRepo[Int, UsersRow] with PostgresProfile {
  import profile.simple._

  val query = Users

  def withId(model: UsersRow, id: Int) = model.copy(id = Some(id))

  def checkCredentials(username: String, password: String): DBReader[Boolean] = DBReader {
    implicit s =>
      query.filter(_.username === username).
        map(_.password).
        run.
        exists(checkPassword(_, password))
  }

  def userExists(username: String): DBReader[Boolean] = DBReader {
    implicit s =>
      query.filter(_.username === username).exists.run
  }

  def checkPassword(encrypted: String, userPass: String) = {
    BCrypt.checkpw(userPass, encrypted);
  }

  def getUser(username: String): DBReader[Option[UsersRow]] = DBReader {
    implicit s =>
      Users.filter(_.username === username).firstOption
  }

}