package models.gen.repositories

import scala.slick.driver.JdbcDriver.simple._
import models.Tables._
import models.DBReader
import models.domain.BaseEntity
import models.domain.TableWithId
import scala.Left
import scala.Right
 
/* Base repository for customized code generator
 * */
abstract class BaseRepo[I:BaseColumnType, T <: BaseEntity[I]] {
  val profile: scala.slick.driver.JdbcDriver
  import profile.simple._

  val query: TableQuery[_ <: Table[T] with TableWithId[I]] //with TableWithId[I]

  def withId(model: T, id: I): T

  def add(model: T): DBReader[Either[String, T]] = DBReader { implicit s =>
    try {
      val id = query.returning(query.map(_.id)).insert(model)
      Right(withId(model, id))
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def update(model: T): DBReader[Either[String, T]] = DBReader { implicit s =>
    try {
      val res = filterById(model.id.get).update(model).run
      if (res > 0)
        Right(model)
      else
        Left("Nothing saved")
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def list: DBReader[List[T]] = DBReader { implicit s => query.list }

  def pagedList(pageIndex: Int, limit: Int): DBReader[List[T]] = DBReader { implicit s =>
    query.drop(pageIndex).take(limit).run.toList
  }

  def filterById(id: I) = query.filter(_.id === id)

  def deleteById(id: I): DBReader[Either[String, Unit]] = DBReader { implicit s =>
    try {
      val res = filterById(id).delete
      Right(())
    } catch {
      case e: Throwable => Left(e.getMessage())
    }
  }

  def findById(id: I): DBReader[Option[T]] = DBReader { implicit s =>
    filterById(id).firstOption
  }
}
