package models.gen.repositories

import models.Tables._
import models.domain.PostgresProfile

import models.DBReader

trait OffersRepo extends BaseRepo[Int, OffersRow] with PostgresProfile {
  import profile.simple._

  val query = Offers

  def withId(model: OffersRow, id: Int) = model.copy(id = Some(id))

  def validWithEmail: DBReader[List[(OffersRow, UsersRow)]] = DBReader {
    implicit s =>
      val res = for {
        offer <- query
        user <- offer.usersFk if (user.enabled === true)
      } yield (offer, user)
      res.list
  }

  def offersByUser(userid: Int): DBReader[List[(OffersRow, String)]] = DBReader {
    implicit s =>
      val res = for {
        offer <- Offers if (offer.userid === userid)
        user <- offer.usersFk if (user.enabled === true)
      } yield (offer, user.email)
      res.list
  }

}