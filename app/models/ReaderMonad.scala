package models

import scala.slick.driver.JdbcDriver.simple._

case class DBReader[A](g: Session => A) {
  def apply(s: Session) = g(s)
  def map[B](f: A => B): DBReader[B] =
    DBReader { s => f(g(s)) }

  def flatMap[B](f: A => DBReader[B]): DBReader[B] =
    DBReader { s => f(g(s)).g(s) }
}