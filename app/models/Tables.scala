package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = scala.slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: scala.slick.driver.JdbcProfile
  import profile.simple._
  import scala.slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import scala.slick.jdbc.{GetResult => GR}
  
  /** DDL for all tables. Call .create to execute. */
  lazy val ddl = Messages.ddl ++ Offers.ddl ++ Users.ddl
  
  /** Entity class storing rows of table Messages
   *  @param subject Database column subject DBType(VARCHAR), Length(100,true)
   *  @param content Database column content DBType(TEXT), Length(65535,true), Default(None)
   *  @param userid Database column userid DBType(INT)
   *  @param sender Database column sender DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class MessagesRow(subject: String, content: Option[String] = None, userid: Int, sender: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching MessagesRow objects using plain SQL queries */
  implicit def GetResultMessagesRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Int], e3: GR[Option[Int]]): GR[MessagesRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<?[String], <<[Int], <<[Int])
    import r._
    MessagesRow.tupled((_2, _3, _4, _5, _1)) // putting AutoInc last
  }
  /** Table description of table messages. Objects of this class serve as prototypes for rows in queries. */
  class Messages(_tableTag: Tag) extends Table[MessagesRow](_tableTag, "messages") with models.domain.TableWithId[Int] {
    def * = (subject, content, userid, sender, id.?) <> (MessagesRow.tupled, MessagesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (subject.?, content, userid.?, sender.?, id.?).shaped.<>({r=>import r._; _1.map(_=> MessagesRow.tupled((_1.get, _2, _3.get, _4.get, _5)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column subject DBType(VARCHAR), Length(100,true) */
    val subject: Column[String] = column[String]("subject", O.Length(100,varying=true))
    /** Database column content DBType(TEXT), Length(65535,true), Default(None) */
    val content: Column[Option[String]] = column[Option[String]]("content", O.Length(65535,varying=true), O.Default(None))
    /** Database column userid DBType(INT) */
    val userid: Column[Int] = column[Int]("userid")
    /** Database column sender DBType(INT) */
    val sender: Column[Int] = column[Int]("sender")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Users (database name fk_messages_users) */
    lazy val usersFk1 = foreignKey("fk_messages_users", userid, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
    /** Foreign key referencing Users (database name fk_messages_users1) */
    lazy val usersFk2 = foreignKey("fk_messages_users1", sender, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Messages */
  lazy val Messages = new TableQuery(tag => new Messages(tag))
  
  /** Entity class storing rows of table Offers
   *  @param text Database column text DBType(TEXT), Length(65535,true)
   *  @param userid Database column userid DBType(INT)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class OffersRow(text: String, userid: Int, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching OffersRow objects using plain SQL queries */
  implicit def GetResultOffersRow(implicit e0: GR[String], e1: GR[Int], e2: GR[Option[Int]]): GR[OffersRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<[Int])
    import r._
    OffersRow.tupled((_2, _3, _1)) // putting AutoInc last
  }
  /** Table description of table offers. Objects of this class serve as prototypes for rows in queries. */
  class Offers(_tableTag: Tag) extends Table[OffersRow](_tableTag, "offers") with models.domain.TableWithId[Int] {
    def * = (text, userid, id.?) <> (OffersRow.tupled, OffersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (text.?, userid.?, id.?).shaped.<>({r=>import r._; _1.map(_=> OffersRow.tupled((_1.get, _2.get, _3)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column text DBType(TEXT), Length(65535,true) */
    val text: Column[String] = column[String]("text", O.Length(65535,varying=true))
    /** Database column userid DBType(INT) */
    val userid: Column[Int] = column[Int]("userid")
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Foreign key referencing Users (database name fk_offers_users1) */
    lazy val usersFk = foreignKey("fk_offers_users1", userid, Users)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Offers */
  lazy val Offers = new TableQuery(tag => new Offers(tag))
  
  /** Entity class storing rows of table Users
   *  @param username Database column username DBType(VARCHAR), Length(60,true)
   *  @param password Database column password DBType(VARCHAR), Length(80,true)
   *  @param enabled Database column enabled DBType(BIT)
   *  @param email Database column email DBType(VARCHAR), Length(80,true)
   *  @param authority Database column authority DBType(VARCHAR), Length(45,true), Default(user)
   *  @param name Database column name DBType(VARCHAR), Length(100,true)
   *  @param id Database column id DBType(INT), AutoInc, PrimaryKey */
  case class UsersRow(username: String, password: String, enabled: Option[Boolean], email: String, authority: String = "user", name: String, id: Option[Int] = None) extends models.domain.BaseEntity[Int]
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[String], e1: GR[Option[Boolean]], e2: GR[Option[Int]]): GR[UsersRow] = GR{
    prs => import prs._
    val r = (<<?[Int], <<[String], <<[String], <<?[Boolean], <<[String], <<[String], <<[String])
    import r._
    UsersRow.tupled((_2, _3, _4, _5, _6, _7, _1)) // putting AutoInc last
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends Table[UsersRow](_tableTag, "users") with models.domain.TableWithId[Int] {
    def * = (username, password, enabled, email, authority, name, id.?) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (username.?, password.?, enabled, email.?, authority.?, name.?, id.?).shaped.<>({r=>import r._; _1.map(_=> UsersRow.tupled((_1.get, _2.get, _3, _4.get, _5.get, _6.get, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column username DBType(VARCHAR), Length(60,true) */
    val username: Column[String] = column[String]("username", O.Length(60,varying=true))
    /** Database column password DBType(VARCHAR), Length(80,true) */
    val password: Column[String] = column[String]("password", O.Length(80,varying=true))
    /** Database column enabled DBType(BIT) */
    val enabled: Column[Option[Boolean]] = column[Option[Boolean]]("enabled")
    /** Database column email DBType(VARCHAR), Length(80,true) */
    val email: Column[String] = column[String]("email", O.Length(80,varying=true))
    /** Database column authority DBType(VARCHAR), Length(45,true), Default(user) */
    val authority: Column[String] = column[String]("authority", O.Length(45,varying=true), O.Default("user"))
    /** Database column name DBType(VARCHAR), Length(100,true) */
    val name: Column[String] = column[String]("name", O.Length(100,varying=true))
    /** Database column id DBType(INT), AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    
    /** Uniqueness Index over (email) (database name email_UNIQUE) */
    val index1 = index("email_UNIQUE", email, unique=true)
    /** Uniqueness Index over (username) (database name username_UNIQUE) */
    val index2 = index("username_UNIQUE", username, unique=true)
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))
}