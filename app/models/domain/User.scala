package models.domain

case class User(
  username: String, password: String, enabled: Option[Boolean], email: String, authority: String, name: String, id: Option[Int])
  extends BaseEntity[Int]

trait UsersTable extends BaseTables {
  import profile.simple._

  class Users(tableTag: Tag) extends Table[User](tableTag, "users") with TableWithId[Int] {
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val username = column[String]("username", O.Length(60, varying = true))
    val password: Column[String] = column[String]("password", O.Length(80,varying=true))
    val enabled: Column[Option[Boolean]] = column[Option[Boolean]]("enabled")
    val email: Column[String] = column[String]("email", O.Length(80,varying=true))
    val authority: Column[String] = column[String]("authority", O.Length(45,varying=true), O.Default("user"))
    val name: Column[String] = column[String]("name", O.Length(100,varying=true))
    
    def * = (username, password, enabled, email, authority, name, id.?) <> (User.tupled, User.unapply)
    def uniqUser = index("uniqUser", username, unique = true)
  }
  
  lazy val usersQuery = TableQuery(tag => new Users(tag))
}