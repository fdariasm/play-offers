package models.domain

trait PostgresProfile {
  val profile = scala.slick.driver.MySQLDriver
}

object Tables extends PostgresProfile
  with UsersTable with MessagesTable with OffersTable