package models.domain

case class Offer(
    text: String, userid: Int, id:Option[Long]
    ) extends BaseEntity[Long]

trait OffersTable extends BaseTables{
  import profile.simple._
 
  class Offers(tableTag: Tag) extends Table[Offer](tableTag, "offers") with TableWithId[Long] {
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    val text: Column[String] = column[String]("text", O.DBType("text"))
    val userid: Column[Int] = column[Int]("username")
    
    def * = (text, userid, id.?) <> (Offer.tupled, Offer.unapply)
    
    lazy val usersFk = foreignKey("fk_offers_users1", userid, Tables.usersQuery)(r => r.id, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  
  lazy val offersQuery = new TableQuery(tag => new Offers(tag))
}