package models.domain

case class Message(
  subject: String, content: Option[String] = None, email: String, userid: Int, id: Option[Int] = None)
  extends BaseEntity[Int] {
}

trait MessagesTable extends BaseTables {
  import profile.simple._

  class Messages(tableTag: Tag) extends Table[Message](tableTag, "messages") with TableWithId[Int] {
    val id = column[Int]("id", O.AutoInc, O.PrimaryKey)
    val subject = column[String]("subject", O.Length(100, varying = true))
    val content: Column[Option[String]] = column[Option[String]]("content", O.DBType("text"), O.Default(None))
    val email: Column[String] = column[String]("email", O.Length(80, varying = true))
    val userid: Column[Int] = column[Int]("userid")

    def * = (subject, content, email, userid, id.?) <> (Message.tupled, Message.unapply)

    lazy val usersFk = foreignKey("fk_messages_users1", userid, Tables.usersQuery)(r => r.id, onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  lazy val messagesQuery = new TableQuery(tag => new Messages(tag))
}

abstract class SubjectObserver {
  type S <: Subject;
  type O <: Observer;
  abstract class Subject { self: S =>
    private var observers: List[O] = List();
    def subscribe(obs: O) =
      observers = obs :: observers;
    def publish =
      for (obs <- observers)
        obs.notify(this);
  }
  abstract class Observer {
    def notify(sub: S): Unit;
  }
}

case class Test(c: Int){
  asd => 
    
  def tas = {
    
    c
  }
}

object asdf{
  def op = {
    val t = Test(9)
    
  }
}