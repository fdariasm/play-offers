package models.repositories

import models.domain._
import models.domain.Tables._
import models.gen.repositories.BaseRepo

trait OffersRepo extends BaseRepo[Long, Offer] with PostgresProfile{
  val query = offersQuery 
  
  def withId(model: Offer, id: Long) = model.copy(id = Some(id))
}