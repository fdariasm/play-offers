package models.repositories

import models.domain._
import models.domain.Tables._
import models.gen.repositories.BaseRepo

trait UsersRepo extends BaseRepo[Int, User] with PostgresProfile{  
  val query = usersQuery 
  def withId(model: User, id: Int) = model.copy(id = Some(id))
  
}