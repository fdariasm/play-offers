package models.repositories

import models.domain._
import models.domain.Tables._
import models.gen.repositories.BaseRepo

trait MessagesRepo extends BaseRepo[Int, Message] with PostgresProfile{
  val query = messagesQuery
  
  def withId(model: Message, id: Int) = model.copy(id = Some(id))
}