package controllers

import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.Messages
import play.api.mvc._
import play.api.Play.current
import play.api.db.DB
import models.services.LoginServiceImp
import models.services.OffersService
import models.services.OffersServiceImp
import views.html.defaultpages.unauthorized

import controllers.FuncHelpers._

import models.gen.repositories.{LoginRepo, OffersRepo}

trait OffersController {
  this: Controller with Secured =>

  def offersService: OffersService

  val offerForm = Form(
    mapping(
      "id" -> optional(number),
      "text" -> nonEmptyText)((id, text) => OffersRow(text, id.getOrElse(0)))((offer: OffersRow) => Some(offer.id, offer.text)))

  def showOffers = actionWithContext { implicit context =>
    Ok(views.html.offers(offersService.list))
  }

  def showOffer(id: Int) = actionWithContext { implicit context =>
    offersService.byId(id).map { o =>
      Ok(views.html.showOffer(offerForm.fill(o)))
    } getOrElse (BadRequest("Invalid offer"))
  }

  def editOffer(id: Int) = withAuth { implicit context =>
    implicit rs =>
      val edit = for {
        user <- context.user
        offer <- offersService.byId(id) if (offer.userid == user.id.get)
      } yield (Ok(views.html.editOffer(offerForm.fill(offer), id)))

      edit getOrElse (BadRequest("Invalid offer"))
  }
  
  def deleteOffer(id: Int) = withAuth { implicit c => rs =>
    val del = for{
      user <- c.user
      offer <- offersService.byId(id) if (offer.userid == user.id.get)
    } yield{
      offersService.delete(offer).fold(
        BadRequest(_),
        _ => Redirect(routes.OffersController.showOffers()).flashing("success" -> Messages("offers.delete.success"))
      )
    }
    
    del getOrElse(Unauthorized)
  }

  //too much logic in a controller
  def updateOffer(id: Int) = withAuth { implicit context =>
    implicit request =>
      val res = for{
        user <- optionToEither(context.user, Unauthorized).right //here user is guaranteed to be present 
        offer <- formToEither(offerForm.bindFromRequest)(fwe => BadRequest(views.html.editOffer(fwe, id))).right
        offerDB <- optionToEither(offersService.byId(id).filter(_.userid == user.id.get), Unauthorized).right
        _ <- offersService.update(offer.copy(id = Some(id), userid = offerDB.userid)).left.map(BadRequest(_)).right
      }yield{
        Redirect(routes.OffersController.showOffers()).flashing("success" -> Messages("offers.create.success"))
      }
      res.fold(identity, identity)
  }

  def createOffer = withAuth { implicit context =>
    implicit request =>
      Ok(views.html.createOffer(offerForm))
  }

  def saveOffer = withAuth { implicit context => implicit request =>
    (for{
      user <- optionToEither(context.user, Unauthorized).right
      offer <- formToEither(offerForm.bindFromRequest)(fwe => BadRequest(views.html.createOffer(fwe))).right
      _ <- offersService.save(offer.copy(userid = user.id.get)).left.map(BadRequest(_)).right
    }yield{
      Redirect(routes.OffersController.showOffers()).flashing("success" -> Messages("offers.create.success"))
    }) fold(identity, identity)
  }
}

object OffersController extends Controller with OffersController with ValidSecured {
  val ds = DB.getDataSource()
  val loginService = new LoginServiceImp(ds, new LoginRepo{})

  val offersService = new OffersServiceImp(ds, new OffersRepo{})
}