package controllers

import play.api._
import play.api.mvc._
import play.api.db._
import play.api.data._
import play.api.data.validation.Constraints._
import play.api.data.Forms._
import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.DBAction
import models.gen.repositories.MessagesRepo
import models.services.LoginServiceImp
import play.api.db.DB
import play.api.Play.current
import models.gen.repositories.LoginRepo
import models.Tables._
import models.services.LoginService
import play.api.i18n.Messages
import FuncHelpers._
import play.api.libs.json._
import play.api.libs.functional.syntax._

trait MessagesController { this: Controller with Secured =>

  def loginService: LoginService

  implicit val msgWriter = Json.writes[MessagesRow]

  def msgsToJson(msgs: List[(MessagesRow, UsersRow)]) = Json.obj(
    "size" -> msgs.size)

  val msgForm = Form(
    mapping(
      "subject" -> nonEmptyText,
      "content" -> text)((s, c) => MessagesRow(s, Some(c), 0, 0))((m: MessagesRow) => Some(m.subject, m.content.getOrElse(""))))

  def msgRepo: MessagesRepo

  def list = withAuthDB { implicit cx =>
    val res = cx.user.map(u => msgRepo.listWithUsers(u.id.get).apply(cx.request.dbSession))
    render {
      case Accepts.Json() => Ok(msgsToJson(res.getOrElse(Nil)))
      case Accepts.Html() => Ok(views.html.listmessages(res.getOrElse(Nil)))
    }
  }

  def newMsg(id: Int) = actionWithContext { implicit cx =>
    loginService.getUser(id).map { _ =>
      val finForm = cx.user.map(u => msgForm.fill(MessagesRow("", None, id, u.id.get))) getOrElse msgForm
      Ok(views.html.newMessage(finForm, id))
    } getOrElse BadRequest(Messages("account.user.nonexist"))
  }

  def createMsg(id: Int) = withAuthDB { implicit cx =>
    val res = for {
      user <- optionToEither(loginService.getUser(id), BadRequest(Messages("account.user.nonexist"))).right
      msg <- formToEither(msgForm.bindFromRequest)(fwe => BadRequest(views.html.newMessage(fwe, id))).right
      _ <- msgRepo.add(msg.copy(userid = id, sender = cx.user.get.id.get)).apply(cx.request.dbSession).left.map(BadRequest(_)).right
    } yield {
      Redirect(routes.OffersController.showOffers()).
        flashing("success" -> Messages("messages.send.success"))
    }
    res.fold(identity, identity)
  }

  implicit val locationReads: Reads[MessagesRow] = (
    (JsPath \ "subject").read[String] and
    (JsPath \ "content").read[String])((s, c) => MessagesRow(s, Some(c), 0, 0))
    

  //TODO improve and possibly merge with createMsg
  def createMsgJson( id: Int ) = withAuthDB { implicit cx =>
    val jsonMsg = cx.body.asJson.getOrElse(JsNull)
    val res = for {
      user <- optionToEither(loginService.getUser(id), BadRequest(Messages("account.user.nonexist"))).right
      msg <- jsonToEither(jsonMsg.validate[MessagesRow](locationReads))(errors =>
        BadRequest(Json.obj("status" -> "ERROR", "message" -> JsError.toFlatJson(errors)))).right
      _ <- msgRepo.add(msg.copy(userid = id, sender = cx.user.get.id.get)).apply(cx.request.dbSession).left.map(BadRequest(_)).right
    } yield {
      Ok(Json.obj("status" ->"OK", "message" -> Messages("messages.send.success")))
    }
    res.fold(identity, identity)
  }
}

object MessagesController extends Controller with MessagesController
  with ValidSecured {

  val loginService = new LoginServiceImp(DB.getDataSource(), new LoginRepo {});

  val msgRepo = new MessagesRepo {}
}