package controllers

import play.api._
import play.api.mvc._
import models.services.LoginService
import models.Tables._
import play.api.db.slick.DBAction
import play.api.db.slick.DBSessionRequest

trait Secured {
  this: Controller =>

  def username(request: RequestHeader): Option[UsersRow]

  def withValidAuth(f: => Context => Request[AnyContent] => Result)(implicit authorities: Set[String]): EssentialAction

  def withAuth(f: => Context => Request[AnyContent] => Result): EssentialAction

  def actionWithContext(f: Context => Result): play.api.mvc.Action[AnyContent]
  
  def withAuthDB(f: => ContextSlick => Result): EssentialAction
  
  def actionWithContextDB(f: => ContextSlick => Result): EssentialAction

  implicit def contextAsSession[_](implicit r: ContextSlick): play.api.db.slick.Session = r.request.dbSession
  
  implicit def contextAsRequest[_](implicit r: ContextSlick): play.api.mvc.Request[AnyContent] = r.request
}

trait ValidSecured extends Secured {
  this: Controller =>

  def loginService: LoginService

  def authorizedUsers(request: RequestHeader)(implicit authorities: Set[String]) = {
    username(request).filter(u => authorities.contains(u.authority))
  }

  def username(request: RequestHeader) = request.session.get(Security.username).flatMap(loginService.getUser(_))

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.LoginController.login)

  def onInvalidCredentials(request: RequestHeader) = BadRequest("Access denied")

  def withValidAuth(f: => Context => Request[AnyContent] => Result)(implicit authorities: Set[String]) = {
    Security.Authenticated(authorizedUsers, onInvalidCredentials) { user =>
      Action(request => f(Context(Some(user), request))(request))
    }
  }

  def withAuth(f: => Context => Request[AnyContent] => Result) = {
    Security.Authenticated(username, onUnauthorized) { user =>
      Action(request => f(Context(Some(user), request))(request))
    }
  }

  def actionWithContext(f: Context => Result) = {
    Action { rs =>
      f(Context(username(rs), rs))
    }
  }
  
  def actionWithContextDB(f: => ContextSlick => Result): EssentialAction =
    DBAction(rs => f(ContextSlick(username(rs), rs)))
    

  def withAuthDB(f: => ContextSlick => Result): EssentialAction =
    Security.Authenticated(username, onUnauthorized) {
      user => DBAction(request => f(ContextSlick(Some(user), request)))
    }
}

trait TestingSecured extends Secured {
  this: Controller =>

  val defaultUser: UsersRow = UsersRow("user", "password", None, email = "email@asdf.com", name = "name", id = Some(1))
  def username(request: RequestHeader) = Some(defaultUser)

  def withValidAuth(f: => Context => Request[AnyContent] => Result)(implicit authorities: Set[String]) =
    Action(request => f(Context(Some(defaultUser), request))(request))

  def withAuth(f: => Context => Request[AnyContent] => Result) = {
    Action(request => f(Context(Some(defaultUser), request))(request))
  }

  def actionWithContext(f: Context => Result) = {
    Action { rs =>
      f(Context(username(rs), rs))
    }
  }
  
  def actionWithContextDB(f: => ContextSlick => Result): EssentialAction =
    DBAction(rs => f(ContextSlick(username(rs), rs)))

  def withAuthDB(f: => ContextSlick  => Result): EssentialAction =
    DBAction(request => f(ContextSlick(Some(defaultUser), request)))
}