package controllers

import play.api.mvc._
import play.api.db.slick.DBSessionRequest
import models.Tables.UsersRow

abstract class BaseContext (val user: Option[UsersRow], val request: Request[AnyContent])
        extends WrappedRequest(request)

case class Context (override val user: Option[UsersRow], override val request: Request[AnyContent])
        extends BaseContext(user, request)

case class ContextSlick(override val user: Option[UsersRow], override val request: DBSessionRequest[AnyContent])
        extends BaseContext(user, request)

case class TestingContext(override val user: Option[UsersRow] = None, override val request: Request[AnyContent])
        extends BaseContext(user, request)