package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current
import play.api.i18n.Messages
import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.DBAction
import models.Tables._
import play.api.db.DB
import models.services.LoginServiceImp
import models.gen.repositories.LoginRepo

trait ApplicationController {
  this: Controller with Secured =>
  
  implicit val authorities = Set("admin")
  
  def adminTest = withValidAuth { implicit context => implicit request =>
    Ok("Hello admin user")
  }
  
  def index = actionWithContext { implicit context =>     
    Ok(views.html.index())
  }
  
}

object Application extends Controller with 
      ApplicationController with 
      ValidSecured {
  
  val loginService = new LoginServiceImp( DB.getDataSource(), new LoginRepo{} )
}