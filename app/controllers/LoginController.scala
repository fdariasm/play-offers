package controllers

import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import play.api._
import play.api.data._
import play.api.data.validation.Constraints._
import play.api.data.Forms._
import play.api.Play.current
import play.api.i18n.Messages
import play.api.mvc._
import play.api.db._
import models.services.LoginService
import models.services.LoginServiceImp

import models.gen.repositories.LoginRepo

trait LoginController  {
  this: Controller with Secured =>

  def loginService: LoginService
    
  val loginForm = Form {
    tuple(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText) verifying (Messages("login.error"), result => result match {
        case (username, password) => loginService.checkCredentials(username, password) 
      })
  }

  val accountForm = Form(
    mapping(
      "username" -> nonEmptyText(minLength = 6, maxLength = 15).verifying(
        Messages("account.create.username"),
        username => username.matches("^\\w{6,}")),
      "password" -> nonEmptyText(minLength = 6).
        verifying(Messages("account.create.password"), pass => pass.matches("^\\S+$")),
      "email" -> email.verifying(nonEmpty),
      "name" -> nonEmptyText(minLength = 6)
      )((username, password, email, name) => UsersRow(username, password, Some(true), email, name = name))
      ((user: UsersRow) => Some(user.username, user.password, user.email, user.name)))

  def login = actionWithContext { implicit request =>
    username(request).
      map(_ => Redirect(routes.Application.index)).
      getOrElse(Ok(views.html.login(loginForm)))
  }
  
  def authenticate = actionWithContext { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.login(formWithErrors)),
      user => Redirect(routes.Application.index).withSession(Security.username -> user._1))
  }

  def logout = Action {
    Redirect(routes.LoginController.login).withNewSession.flashing(
      "success" -> Messages("login.logout"))
  }

  def newAccount = actionWithContext { implicit context =>
    Ok(views.html.createAccount(accountForm))
  }

  def createAccount = actionWithContext { implicit context =>
    accountForm.bindFromRequest.fold(
      withErrors => BadRequest(views.html.createAccount(withErrors)),
      nUser => {
        if (loginService.userExists(nUser.username)) {
          val formWithError = accountForm.fill(nUser).withError("username", Messages("account.create.user.exists"))
          BadRequest(views.html.createAccount(formWithError))
        } else {
          loginService.saveUser(nUser)
          Redirect(routes.Application.index).
            withSession(Security.username -> nUser.username).
            flashing("success" -> Messages("account.create.success"))
        }
      })
  }
}

object LoginController extends Controller with 
        LoginController with 
        ValidSecured {
  
  val loginService = new LoginServiceImp( DB.getDataSource(), new LoginRepo{})
}