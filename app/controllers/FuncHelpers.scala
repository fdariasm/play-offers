package controllers

import play.api.data.Form

object FuncHelpers {
  def defOption[A, B](o: Option[A], b: => B)(f: A => B): Option[B] = o match {
    case Some(a) => Some(f(a))
    case None => Some(b)
  }

  def optionToEither[A, B](o: Option[A], v: => B): Either[B, A] = o match {
    case Some(a) => Right(a)
    case None => Left(v)
  }

  def formToEither[A, B](f: Form[A])(g: => Form[A] => B): Either[B, A] = f.fold(
    fwe => Left(g(fwe)),
    a => Right(a))

  type JsonError = Seq[(play.api.libs.json.JsPath, Seq[play.api.data.validation.ValidationError])]

  def jsonToEither[A, B](j: play.api.libs.json.JsResult[A])(f: JsonError => B) =
    j.fold(
      errors => Left(f(errors)),
      value => Right(value))
}