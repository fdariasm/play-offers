$(document).ready(function(){	
	$(".replybutton").click(function(){
		var textid = $(this).attr("data-msgid");
		var textArea = "#reply"+textid;
		var content = $(textArea).val();
		var userid = $(this).attr("data-userid");
		var subject = "reply: " + $(this).attr("data-subject");
		$.ajax({
			"type": "POST",
			"async": false,
			"url": "/newmsgjson/" + userid,
			"data": JSON.stringify({ "subject": subject, "content" : content }),
			"success": function(data){
				console.log(data);
				$.notify(data.message, "success");
			},
			"error": function(xhr, data, error){
				$.notify("An error occured when sending the message", "error");
				console.log(data);
				console.log(xhr);
				console.log(error);
			},
			contentType: "application/json",
			dataType: 'json'
		});
	});
});