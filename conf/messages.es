login.error=Usuario o contraseña inválidos
login.logout=Ha cerrado la sesion correctamente

account.create.username=Nombre de usuario solo puede contener letras, números y guión bajo
account.create.password=Las contraseñas no pueden contener espacios
account.create.user.exists=Nombre de usuario ya existe
account.create.success=Usuario creado
account.create.dontmatch=Contraseñas no coinciden
account.create.create=Crear nueva cuenta
account.user.nonexist=Usuario no existe

offers.create.success=Oferta guardada
offers.delete.success=Se ha eliminado la oferta
offers.delete.confirm=Esta seguro que desea eliminar la oferta?

error.required = Campo requerido
error.minLength=Longitud mínima de {0} carácteres
error.maxLength=Longitud máxima de {0} carácteres

messages.send.success=El mensaje se ha enviado