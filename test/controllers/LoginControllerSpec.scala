package controllers

import play.api.mvc._
import play.api.test._
import play.api.mvc.Results._
import play.test.FakeRequest._
import org.specs2.mock._
import models.services.LoginService
import javax.security.auth.login.LoginContext
import models.Tables._

class LoginControllerSpec extends PlaySpecification with Mockito{
  val loginServiceMock = mock[LoginService]
  
  object loginControllerTest extends Controller with LoginController with TestingSecured{
    val loginService = loginServiceMock
  }
  val username = "username"
  val password = "password"
  val loginInfo = List(("username", username), ("password", password))
  
  "LoginController#login" should{
    "Redirect to index if already logged in" in new WithApplication{
      val result = loginControllerTest.login.apply(FakeRequest())
      redirectLocation(result) must beSome(routes.Application.index.url)
    }
  }
  
  "LoginController#authenticate" should{
    "Report bad request on unsuccessful login" in new WithApplication{
      loginServiceMock.checkCredentials("user", "password") returns false
      val request = FakeRequest().withFormUrlEncodedBody(loginInfo: _*)
      val result = loginControllerTest.authenticate.apply(request)
      status(result) mustEqual BAD_REQUEST
    }
    
    "Redirect to index on successful login" in new WithApplication{
      loginServiceMock.checkCredentials(username, password) returns true
      val request = FakeRequest().withFormUrlEncodedBody(loginInfo: _*)
      val result = loginControllerTest.authenticate.apply(request)
      redirectLocation(result) must beSome(routes.Application.index.url)
    }
  }
  
  val newUser = loginInfo :+ ("email", "asdf@qwerty.com") :+ ("name", "qwertyu")
  
  "LoginController#createAccount" should{
    "Report bad request on insufficient information" in new WithApplication{
      val request = FakeRequest().withFormUrlEncodedBody(("username", "user"))
      val result = loginControllerTest.createAccount.apply(request)
      status(result) mustEqual BAD_REQUEST
    }
    
    "Report bad request on existent user" in new WithApplication{
      loginServiceMock.userExists(username) returns true
      val request = FakeRequest().withFormUrlEncodedBody(newUser: _*)
      val result = loginControllerTest.createAccount.apply(request)
      status(result) mustEqual BAD_REQUEST
    }
    
    "Redirect to index on successful creation" in new WithApplication{
      loginServiceMock.userExists(username) returns false
      
      val request = FakeRequest().withFormUrlEncodedBody(newUser: _*)
      val nUser = LoginController.accountForm.bindFromRequest()(request).get
      val result = loginControllerTest.createAccount.apply(request)
      there was one(loginServiceMock).saveUser(nUser)
      redirectLocation(result) must beSome(routes.Application.index.url)
    }
  }
}
