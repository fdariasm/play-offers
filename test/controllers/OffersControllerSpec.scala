package controllers

import play.api.mvc._
import play.api.test._
import play.api.mvc.Results._
import play.test.FakeRequest._
import org.specs2.mock._
import models.services.LoginService
import javax.security.auth.login.LoginContext
import models.Tables._
import models.services.OffersService

class OffersControllerSpec extends PlaySpecification with Mockito{
  
  val offersServMock = mock[OffersService]
  val loginServiceMock = mock[LoginService]
  
  object ControllerTest extends Controller with OffersController with TestingSecured{
    val offersService = offersServMock
    val loginService = loginServiceMock
  } 
  
  "OffersController#saveOffer" should{
    "Report bad request on incomplete data" in new WithApplication{
      val result1 = call(ControllerTest.saveOffer, FakeRequest("", POST).withFormUrlEncodedBody())
      
      status(result1) mustEqual BAD_REQUEST
    }
    
    "Save correct offer and redirect to list of offers" in new WithApplication{
      implicit val request = FakeRequest("", POST).withFormUrlEncodedBody(("id", "0"),
          ("text", "My description"))
      
      val offer = OffersController.offerForm.bindFromRequest().get.copy(userid = 1)
      offersServMock.save(offer) returns Right(offer)
      
      val result = call(ControllerTest.saveOffer, request)
      
      there was one(offersServMock).save(offer)
      redirectLocation(result) must beSome(routes.OffersController.showOffers.url)
    }
  }
  
  "OffersController$editOffer" should{
    "Report bad request on incorrect id" in new WithApplication{
      val result = call(ControllerTest.editOffer(3), FakeRequest())
      offersServMock.byId(3) returns (None)
      status(result) mustEqual BAD_REQUEST
    }
  }

}