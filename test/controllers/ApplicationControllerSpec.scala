package controllers

import play.api.mvc._
import play.api.test._
import play.api.mvc.Results._
import play.api.libs.json.Json
import play.test.FakeRequest._

object ApplicationControllerSpec extends PlaySpecification {

  class TestController extends Controller with ApplicationController with TestingSecured

  "Application Page#index" should {

    "Be valid" in new WithApplication{ //WithApplication required
      val controller = new TestController()
      //      val result = call(controller.index, FakeRequest())
      val result = controller.index.apply(FakeRequest())
      status(result) mustEqual OK
    }

    "Render index template" in new WithApplication {
      implicit val context = TestingContext(None, FakeRequest())
      val html = views.html.index()

      contentAsString(html) must contain("Play Offers Application")
    }

    "Respond to the index Action" in new WithApplication {
      val Some(result) = route(FakeRequest())

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
      contentAsString(result) must contain("Play Offers Application")
    }

    "Run in a Browser" in new WithBrowser(webDriver = WebDriverFactory(FIREFOX), app = FakeApplication()) {
      browser.goTo("/")
      browser.$(".title a").getTexts().get(0) must equalTo("Play Offers Application")
      browser.$(".login a").click()
      browser.url must equalTo("/login")
    }
  }

}