package specs

import play.api.test._
import org.specs2.specification.Step
import org.specs2.specification.Fragments
import play.api.Play

//class OffersSpec extends PlaySpecification {
//
//  lazy val app = FakeApplication()
//  lazy val webdriver = WebDriverFactory(FIREFOX)
//
//  override def is = args(sequential = true) ^
//    """Testing Offers as user""" ^
//    "When I go to main page" ^
//    "I should be able to click on a link to create new offer, and go to the login page" ! f1 ^
//    "Then I should be able to create a new account" ! f2 ^
//    "Once I'm logged in, I should be able to see the create new offer form" ! f3 ^
//    """When I fill in the new form with valid information, then I should see in the offer's list
//    my new offer listed, next to the "created offer" message""" ! f4
//
//  def f1 = new WithBrowser(webDriver = webdriver, app = app) {
//    browser.goTo("/")
//    browser.$("a[href*='create']").click();
//    browser.url must equalTo("/login")
//  }
//
//  def f2 = new WithBrowser(webDriver = webdriver, app = app) {
//    //      browser.$("#username").text("")
//    browser.$("a[href*='createaccount']").click();
//    browser.url must equalTo("/createaccount")
//  }
//
////    def f2 = pending
//
//  def f3 = pending
//
//  def f4 = pending
//
//}
//
//trait BeforeAllAfterAll extends PlaySpecification {
//  // see http://bit.ly/11I9kFM (specs2 User Guide)
//  override def map(fragments: => Fragments) = {
//    beforeAll()
//    fragments ^ Step(afterAll)
//  }
//
//  def beforeAll()
//  def afterAll()
//}

/*
 *     "Run in a Browser" in new WithBrowser(webDriver = WebDriverFactory(FIREFOX), app = FakeApplication()) {
      browser.goTo("/")
      browser.$(".title a").getTexts().get(0) must equalTo("Play Offers Application")
      browser.$(".login a").click()
      browser.url must equalTo("/login")
    }
 * 
 * */