package models

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import play.api.db.DB
import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import org.specs2.specification.BeforeExample
import org.specs2.execute.AsResult
import org.specs2.execute.Result
import models.services.OffersService
import models.services.OffersServiceImp
import models.services.LoginServiceImp
import org.specs2.execute.Pending

import models.gen.repositories._

trait WithLogin {
  this: WithDbData =>

  val loginService = new LoginServiceImp(ds, new LoginRepo{})

  def saveTestUser(username: String = "user", password: String = "password", email: String = "correo@asd.com") = {
    val user = UsersRow(username, password, Some(true), email, "User", "name")
    loginService.saveUser(user)
  }
}

class LoginServiceSpec extends PlaySpecification {

  "LoginService.saveUser" should {
    "Persists the given entity" in new WithDbData with WithLogin {
      val saved = saveTestUser().right.get
      slickDb.withSession { implicit s =>
        Users.list must contain(saved)
      }
    }
  }

  "LoginService.checkCredentials" should {
    "Return false when bad credentials" in new WithDbData with WithLogin{
      val username = "user"
      val password = "12345"
      val saved = saveTestUser(username, password)

      loginService.checkCredentials("nonexistent", "badpassword") must beFalse
    }

    "Return false when bad password" in new WithDbData with WithLogin  {
      val username = "user"
      val password = "12345"
      val saved = saveTestUser(username, password)

      loginService.checkCredentials(username, "badpassword") must beFalse
    }

    "Return true when good credentials" in new WithDbData with WithLogin {
      val username = "user"
      val password = "12345"
      val saved = saveTestUser(username, password)

      loginService.checkCredentials(username, password) must beTrue
    }
  }

  "LoginService.userExists" should {
    "Return false on nonexistent user" in new WithDbData with WithLogin{
      loginService.userExists("nonexistent") must beFalse
    }

    "Return true on existent user" in new WithDbData with WithLogin{
      val username = "user"
      saveTestUser(username)
      loginService.userExists(username) must beTrue
    }
  }
  "LoginService.getUser" should {
    "Return None on nonexistent username" in new WithDbData with WithLogin {
      val username = "user"
      loginService.getUser(username) must beEqualTo(None)
    }

    "Return Some valid user on valid username" in new WithDbData with WithLogin{
      val username = "user"
      val saved = saveTestUser(username).right.get
      loginService.getUser(username) must beSome(saved)
    }
  }
}