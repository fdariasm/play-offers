package models

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import play.api.db.DB
import scala.slick.driver.MySQLDriver.simple._
import models.Tables._
import org.specs2.specification.BeforeExample
import org.specs2.execute.AsResult
import org.specs2.execute.Result
import models.services.OffersService
import models.services.OffersServiceImp

import models.gen.repositories.OffersRepo

abstract class WithDbData extends WithApplication {
  def ds = DB.getDataSource("test")
  def slickDb = Database.forDataSource(ds)
  override def around[T: AsResult](t: => T): Result = super.around {
    setup
    val result = AsResult.effectively(t)
    teardown
    result
  }

  def setup = {
    slickDb.withSession { implicit session =>
      //      ddl.create
      Offers.delete.run
      Users.delete.run
    }
  }

  def teardown = {
    slickDb.withSession { implicit session =>

    }
  }
}

trait WithOffersService {
  this: WithDbData =>
  val offersService = new OffersServiceImp(ds, new OffersRepo {})

  def saveOffer(offer: OffersRow) = offersService.save(offer)
}

class OffersServiceSpec extends PlaySpecification {

  "OffersService#save" should {
    "Persist the given entity" in new WithDbData with WithOffersService with WithLogin {
      val user = saveTestUser()
      val e = OffersRow("description", user.right.get.id.get)
      saveOffer(e).fold(
          error => anError,
          offer => slickDb.withSession { implicit session =>
            Offers.list must contain(offer)
          }
      )
    }
  }
  
  "OffersService#update" should{
    "Update an entity" in new WithDbData with WithOffersService with WithLogin {
      val user = saveTestUser()
      val newDescr = "new description"
      val res = for{
        user <- user.right
        offer <- saveOffer(OffersRow("description", user.id.get)).right
        offerSaved <- offersService.update(offer.copy(text = newDescr)).right
      }yield{
        offersService.byId(offerSaved.id.get)
      }
      res.fold(
          error => anError,
          _.map(_.text) must beSome( newDescr )
      )
    }
  }
  
  "OffersService#delete" should{
    "Delete the given entity" in new WithDbData with WithOffersService with WithLogin {
      val res = for{
        user <- saveTestUser().right
        of1 <- saveOffer(OffersRow("description1", user.id.get)).right
        of2 <- saveOffer(OffersRow("description2", user.id.get)).right
        _ <- offersService.delete(of2).right
      }yield{
        (offersService.list.map(_._1), of2)
      }
      res match{
        case Left(_) => anError
        case Right((l, o)) => l must not contain(o)
      }
    }
  }

  "OffersService.byId" should {
    "Find an entity by id" in new WithDbData with WithOffersService with WithLogin {
      val res = for{
        user <- saveTestUser().right
        offer <- saveOffer(OffersRow("description", user.id.get)).right
      }yield{
        offersService.byId(offer.id.get) must beSome(offer)
      }
      res.fold(_ => anError, identity)
    }
  }

  "OffersService#offers" should {
    "Find all the offers for a given username" in new WithDbData with WithOffersService with WithLogin {
      
      val res = for{
        u1 <- saveTestUser().right
        u2 <- saveTestUser("user2", "password", "mail@asd.com").right
        o1 <- saveOffer( OffersRow("description1", u1.id.get) ).right
        o2 <- saveOffer( OffersRow("description2", u1.id.get) ).right
        o3 <- saveOffer( OffersRow("description3", u2.id.get) ).right
        offers <- Right(offersService.offers(u1.id.get).map(_._1)).right
      }yield{
        offers must have size (2)
        offers must contain(o1, o2)
        offers must not contain(o3)
      }
      
      res.fold(_=> anError, identity)
    }
  }
}