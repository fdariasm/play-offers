package models
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = scala.slick.driver.MySQLDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: scala.slick.driver.JdbcProfile
  import profile.simple._
  import scala.slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import scala.slick.jdbc.{GetResult => GR}
  
  /** DDL for all tables. Call .create to execute. */
  lazy val ddl = Offers.ddl
  
  /** Entity class storing rows of table Offers
   *  @param id Database column id AutoInc, PrimaryKey
   *  @param name Database column name 
   *  @param email Database column email 
   *  @param text Database column text  */
  case class OffersRow(id: Option[Int] = None, name: String, email: String, text: String)
  /** GetResult implicit for fetching OffersRow objects using plain SQL queries */
  implicit def GetResultOffersRow(implicit e0: GR[Int], e1: GR[String]): GR[OffersRow] = GR{
    prs => import prs._
    OffersRow.tupled((<<[Int], <<[String], <<[String], <<[String]))
  }
  /** Table description of table offers. Objects of this class serve as prototypes for rows in queries. */
  class Offers(tag: Tag) extends Table[OffersRow](tag, "offers") {
    def * = (id, name, email, text) <> (OffersRow.tupled, OffersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, name.?, email.?, text.?).shaped.<>({r=>import r._; _1.map(_=> OffersRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    
    /** Database column id AutoInc, PrimaryKey */
    val id: Column[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name  */
    val name: Column[String] = column[String]("name")
    /** Database column email  */
    val email: Column[String] = column[String]("email")
    /** Database column text  */
    val text: Column[String] = column[String]("text")
  }
  /** Collection-like TableQuery object for table Offers */
  lazy val Offers = new TableQuery(tag => new Offers(tag))
}